/*
Copyright (c) 2021, Picpass solutions
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the Picpass solutions nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Picpass solutions BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
export class UserCredential {
  public userName: string;
  public password: string;
  public configFile: string;

  constructor(name: string, pwd: string) {
      this.userName = name;
      this.password = pwd;
      this.configFile = "assets/" + name + ".json";
  }
}
export class PicpassWidget {
  private static _picpassConfig: Object = {};
  private static _instance: PicpassWidget = <PicpassWidget>null;
  private static _modalObject: any;
  private static _initialPassword: string;
  public static credentials: UserCredential;
  private static _childWindow: Window;
  public static picpassWidgetUrl: string = `https://picpass1.northeurope.cloudapp.azure.com:8888`;

  public static getInstance() {
      if (PicpassWidget._instance === null)
          PicpassWidget._instance = new PicpassWidget(this.credentials);
      return PicpassWidget._instance;
  }

  public static sendMessage(text: string) {
    PicpassWidget._childWindow.postMessage({
      "topic": "apply-password-text",
      "message": text
    }, "*");
  }

  public static open() {
      PicpassWidget.getInstance();
      PicpassWidget.openDialog();
  }

  private static _sendConfigRepo(data: object) {
    PicpassWidget._picpassConfig = data;
    const passwordElement: HTMLInputElement = document.querySelector(`#password-input-field`);
    PicpassWidget._initialPassword = passwordElement.value;
    PicpassWidget._picpassConfig['Initiation']['PasswordInitalValue'] = passwordElement.value;
    PicpassWidget._picpassConfig['Initiation']['Password'] = (<HTMLElement>document.querySelector('#password-1')).innerText;
    PicpassWidget._childWindow.postMessage({
        "topic": "register",
        "message": PicpassWidget._picpassConfig
    }, "*");
}

  private constructor(currentUser: UserCredential) {

      window.addEventListener('message', (event: any) => {
          console.log(`parent received message!:  '${event.data.topic}'`);
          let messageTopic: string = event.data.topic;
          switch (messageTopic) {

              case 'submit-password':
                  // console.log(`handling submit-password message`);
                  PasswordTester._testCredentials(null);
                  break;
              case 'register':
                  PicpassWidget._childWindow = event.source;
                  // fetch the current configuration then send it to child page
                  fetch(PicpassWidget.credentials.configFile)
                      .then(response => response.json())
                      .then(data => PicpassWidget._sendConfigRepo(data));
                  // console.log(`handling register message`);
                  break;
              case 'dialog-close':
                  // console.log(`handling dialog-close message`);
                  PicpassWidget.closeDialog();
                  break;
              case 'apply-password-text':
                  // console.log(`handling apply-password-text message`);
                  PicpassWidget.applyPasswordText(<string>event.data.message);
                  break;
              default:
                break;
          }
      });

      const passwordElement: HTMLInputElement = document.querySelector(`#password-input-field`);
      PicpassWidget._initialPassword = passwordElement.value;
  }

  public static openDialog() {
    let picpassIframe: HTMLIFrameElement = document.querySelector(`#picpass-iframe`);
    const testFrameElm: HTMLElement = document.querySelector(`#test-frame`);
    const picpassIframeStyle: CSSStyleDeclaration = window.getComputedStyle(picpassIframe);
    const testFrame: HTMLElement= document.querySelector(`#test-frame`);
    const picpassIframeParent: HTMLElement = picpassIframe.parentElement;
    if (picpassIframeStyle.display === 'none') {
      testFrame.style.display = 'none';
    }
    picpassIframe.remove();

    const testFrameWidthNum = testFrameElm.getBoundingClientRect().width;
    const testFrameHeightNum = testFrameElm.getBoundingClientRect().height;

    const pageHeaderElem: HTMLElement = document.querySelector(`#page-header`);
    const pageHeaderHeightNum: number = pageHeaderElem.getBoundingClientRect().height;


    picpassIframe = document.createElement(`iframe`);
    picpassIframe.id = `picpass-iframe`;
    picpassIframe.width = `${window.innerWidth - testFrameWidthNum}px`;
    picpassIframe.height = (testFrameHeightNum === 0) ? `${window.innerHeight - pageHeaderHeightNum - 4}px`: `${testFrameHeightNum}px`;
    picpassIframe.style.display = 'block';
    picpassIframeParent.insertBefore(picpassIframe, testFrameElm);


    picpassIframe.src = PicpassWidget.picpassWidgetUrl;
    PicpassWidget._childWindow = picpassIframe.contentWindow;
// debugger;
    PicpassWidget._childWindow.location.assign(picpassIframe.src);
    PicpassWidget.sendMessage('opened dialog');
  }

  public static applyPasswordText(inText: string) {
    const passwordElement: HTMLInputElement = document.querySelector(`#password-input-field`);
    passwordElement.value = inText;
      PicpassWidget._childWindow.postMessage({
          "topic": "apply-password-text",
          "message": {}
      }, "*");
      PicpassWidget.sendMessage('apply password text');
  }

  public static closeDialog() {
      const picpassIframe: HTMLIFrameElement = document.querySelector(`#picpass-iframe`);
      picpassIframe.src = `assets/picpass-iframe-out.html`;

      const testFrame: HTMLElement= document.querySelector(`#test-frame`);
      const testFrameStyle: CSSStyleDeclaration = window.getComputedStyle(testFrame);
      if (testFrameStyle.display === 'none') {
        picpassIframe.style.display = 'none';
        testFrame.style.display = 'block';
      } else {
        window.frames[0].location.assign(picpassIframe.src);
      }
      PicpassWidget.sendMessage('close dialog');

      const passwordElement: HTMLInputElement = document.querySelector(`#password-input-field`);
      passwordElement.focus();
  }

  private static _resetPassward() {
    const passwordElement: HTMLInputElement = document.querySelector(`#password-input-field`);
    passwordElement.value = PicpassWidget._initialPassword;
  }
}
class PasswordTester {
  private static _instance: PasswordTester = <PasswordTester>null;
  private static _userCredentialsArr: UserCredential[] = [];
  private static _responseHtmlMessage: string = "";
  private static _testResult: Boolean = false;
  private static _passwordVisble: Boolean = false;

  public static getInstance() {
      if (PasswordTester._instance === null)
          PasswordTester._instance = new PasswordTester();
      return PasswordTester._instance;
  }

  public static sendMessage(text: string) {
      console.log(`PasswordTester > Sent message: ${text}`);
  }

  public static log(msg: string) {console.log(`${new Date().toISOString()} ${msg}`)}

  public static _testCredentials(ev: Event) {
      let inputUser: string = (<HTMLInputElement>document.querySelector('#user-input-field')).value;
      let inputPassword: string = (<HTMLInputElement>document.querySelector('#password-input-field')).value;
      PasswordTester._responseHtmlMessage = `<span class="text-danger">Unknown user <span class="picpass-font bg-e">${inputUser}</span>.</span>`;
      PasswordTester._testResult = false;

      for (let ceredentialObj of PasswordTester._userCredentialsArr) {
          if (ceredentialObj.userName === inputUser
              && ceredentialObj.password === inputPassword) {
              PasswordTester._responseHtmlMessage = `<span class="text-success">Looks good.</span>`;
              PasswordTester._testResult = true;
              break;
          }
          if (ceredentialObj.userName === inputUser
              && ceredentialObj.password !== inputPassword) {
              PasswordTester._responseHtmlMessage = `<span class="text-danger">Expected password: <span class="picpass-font bg-e">${ceredentialObj.password}</span> received: <span class="picpass-font bg-e">${inputPassword}</span> .</span>`;
              break;
          }
      }
      (<HTMLElement>document.querySelector('#testResult')).innerHTML = <string>PasswordTester._responseHtmlMessage;
      (<HTMLInputElement>document.querySelector('#password-input-field')).focus();
  }

  public static onPasswordDoubleClicked(event: Event) {
    PasswordTester.log(`double clicked in password field`);

    const userSelectorElm: HTMLSelectElement = document.querySelector('#user-input-field');
    const currentUserName: string = userSelectorElm.value;
    const currentUserCredentials = PasswordTester._userCredentialsArr.find(cred => cred.userName === currentUserName);
    PicpassWidget.credentials = currentUserCredentials;
    PicpassWidget.getInstance();
    PicpassWidget.openDialog();

  }
  private static _onEnterKeyReleased(event: KeyboardEvent) {
      if (event.keyCode === 13) {
          // PasswordTester.log(`entry key released`);
          event.preventDefault();
          PasswordTester._testCredentials(event);
      }
  }

  public static onTogglePasswordVivibility(event: Event) {
      let passwordVisibilityIcon: HTMLElement = document.querySelector('#password-visibility-icon');
      let passwordInputElement: HTMLInputElement = document.querySelector('#password-input-field');
      if (PasswordTester._passwordVisble === true) {
          passwordVisibilityIcon.classList.remove('fa-eye');
          passwordVisibilityIcon.classList.add('fa-eye-slash');
          passwordInputElement.type = 'password';
          PasswordTester._passwordVisble = false;
      } else {
          passwordVisibilityIcon.classList.remove('fa-eye-slash');
          passwordVisibilityIcon.classList.add('fa-eye');
          passwordInputElement.type = 'text';
          PasswordTester._passwordVisble = true;
      }
      passwordInputElement.focus();
  }

  private constructor() {
      let passwordInputElement: HTMLInputElement = document.querySelector('#password-input-field');
      window.addEventListener('keyup', function(e){
          if(e.key=='Escape'||e.key=='Esc'||e.keyCode==27) {
              e.preventDefault();
              console.log("ESC released");
              // PicpassWidget.cancelDialog();
          }
      }, true);
      passwordInputElement.addEventListener('keyup', PasswordTester._onEnterKeyReleased);
      passwordInputElement.focus();

      document.querySelector('#tester-button').addEventListener('click', PasswordTester._testCredentials);
      document.querySelector('#password-visibility-div').addEventListener('click', PasswordTester.onTogglePasswordVivibility);
      document.querySelector('#password-input-field').addEventListener('dblclick', PasswordTester.onPasswordDoubleClicked);

      const passwordText: string = (<HTMLElement>document.querySelector('#password-1')).innerText;
      const userNamesSpanElmArr: NodeListOf<HTMLSpanElement> = document.querySelectorAll('section>span');
      const userSelectorElm: HTMLSelectElement = document.querySelector('#user-input-field');
      userNamesSpanElmArr.forEach(userNameElm => {
        const userName: string = userNameElm.innerText;
        PasswordTester._userCredentialsArr.push(new UserCredential(userName, passwordText));
        const optionElm: HTMLOptionElement = document.createElement(`option`);
        optionElm.value = userName;
        optionElm.innerText = userName;
        userSelectorElm.append(optionElm);
      });

      passwordInputElement.focus();
  }
}


window.addEventListener('load', () => {
  // When window loaded ( external resources are loaded too- `css`,`src`, etc...)
  PasswordTester.getInstance();
});


