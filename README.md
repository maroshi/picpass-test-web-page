# Picpass

A functional testing website for Picpass solution.

## Installation

1. Install `node.js` into your operating system from https://nodejs.org/en/download/
2. Install http-server web server: Run command line `npm install --global http-server`, read more in https://www.npmjs.com/package/http-server 
3. Install `git` into your operating system from https://git-scm.com/downloads 
4. Install application repository: In a new empty directory run command line `git clone https://maroshi@bitbucket.org/maroshi/picpass-test-web-page.git`
5. Build development repository: Change directory into `picpass-test-web-page.git` run command line `npm install` .

## Development monitor

Run `npm run start:dev` for a development monitor. Web application will be rebuilt on any code change. Use development environment web server to review web page `test-1.html`. 

We use VS-Code plugin `Live Server` for development web server.

## Configure default behavior for subsribers/users

__Edit configuration files:__ `src/assets/administrator.json` `src/assets/exclusive-srubscriber.json` `src/assets/premium-srubscriber.json` `src/assets/community-srubscriber.json` for the respective subscriber/user.

__Edit default common password:__ `src/test-1.html` line 45.

## Build

Only for first time build, give execution permission to build script `build.sh`! Run command line `chmod a+x build.sh`.

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. 

## Publish

Run `npm run publish` to publish the production project on localhost. Automatically open test web page `http://localhost:7777/test-1.html` in default browser.

## Further help
See Picpass soulution site documentation http://homeil.org/documentation
Or contact us from http://homeil.org/contact-us/

Please report problems/bugs in https://bitbucket.org/maroshi/picpass-test-web-page/issues